/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 255;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/

const app = document.getElementById('app');

function addStyleAppDiv(){
  app.style.margin = 0;
  app.style.paddingTop = 50 + 'px';
  app.style.paddingBottom = 50 + 'px';
  app.style.paddingLeft = 0;
  app.style.paddingRight = 0;
}

function createNewDiv(){
  const newDiv = document.createElement('div');
  newDiv.className = 'box';
  newDiv.innerHTML = '<span class="colorText">#f00</span>';
  newDiv.style.width = 300 + 'px';
  newDiv.style.height = 300 + 'px';
  newDiv.style.backgroundColor = '#f00';
  newDiv.style.marginTop = 0;
  newDiv.style.marginBottom = 0;
  newDiv.style.marginRight = 'auto';
  newDiv.style.marginLeft = 'auto';
  newDiv.style.display = 'flex';
  newDiv.style.alignItems = 'center';

  app.appendChild(newDiv);
  styleColorText('f00');
}
function styleColorText(text){
  const colorText = document.querySelector('.colorText');
  colorText.style.marginTop = 0;
  colorText.style.marginBottom = 0;
  colorText.style.marginRight = 'auto';
  colorText.style.marginLeft = 'auto';
  colorText.style.backgroundColor = '#fff';
  colorText.style.paddingTop = 5 + 'px';
  colorText.style.paddingBottom = 5 + 'px';
  colorText.style.paddingLeft = 5 + 'px';
  colorText.style.paddingRight = 5 + 'px';
  colorText.innerText = '#' + text;

}

function createButton(){
  const buttonChange = document.createElement('button');
  buttonChange.className = 'change';
  buttonChange.innerText = 'Change color';
  buttonChange.style.marginTop = 30 + 'px';
  buttonChange.style.marginBottom = 0;
  buttonChange.style.marginRight = 'auto';
  buttonChange.style.marginLeft = 'auto';
  buttonChange.style.display = 'table';
  app.appendChild(buttonChange);
}

addStyleAppDiv();
createNewDiv();
createButton();

const button = document.querySelector('button');
button.addEventListener('click', generateRandomColor);

function generateRandomColor(){

  const randomColor = Math.floor(Math.random()*16777215).toString(16);
  const box = document.querySelector('.box');
  box.style.backgroundColor = '#' + randomColor;

  styleColorText(randomColor);
}

window.reload(generateRandomColor());